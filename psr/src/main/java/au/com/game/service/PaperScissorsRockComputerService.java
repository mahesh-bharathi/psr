package au.com.game.service;

import au.com.game.model.Player;
import au.com.game.utils.PlayerFactory;
import au.com.game.utils.PlayerType;

import java.util.Random;
import java.util.Scanner;

public class PaperScissorsRockComputerService extends AbstractPaperScissorsRockService {

    @Override
    public void playGame() {
        showRules();
        try(Scanner scanner = new Scanner(System.in)) {
            System.out.println(PLAYER_NAME);
            final String name = scanner.nextLine();
            final Player computer = PlayerFactory.getPlayer(PlayerType.SYSTEM, null);
            final Player player = PlayerFactory.getPlayer(PlayerType.HUMAN, name);
            boolean play = true;

            while (play) {
                System.out.println(player.getName() + MOVE);
                String value = scanner.next();

                while (!VALID_MOVE.contains(value.toUpperCase())) {
                    System.out.println(INVALID_MOVE);
                    value = scanner.next();
                }

                player.setValue(value);
                computer.setValue(computerMove());

                if (player.getValue().equalsIgnoreCase(computer.getValue())) {
                    System.out.println(DRAW);
                } else {
                    System.out.println(findWinner(player, computer) + WON);
                }

                System.out.println(PLAY_AGAIN);
                String playAgain = scanner.next();
                if(!playAgain.equalsIgnoreCase(YES)) {
                    play = false;
                    System.out.println(THANKS_FOR_THE_GAME_BYE);
                }
            }
        }
    }

    private String computerMove() {
        final Random random = new Random();
        final int randomNumber = random.nextInt(4);

        String move = null;
        if (randomNumber == 1) {
            move = ROCK;
        } else if (randomNumber == 2) {
            move = PAPER;
        } else {
            move = SCISSORS;
        }
        return move;
    }

}


