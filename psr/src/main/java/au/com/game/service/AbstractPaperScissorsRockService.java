package au.com.game.service;

import au.com.game.model.Player;

import java.util.List;

import static java.util.Arrays.asList;

public abstract class AbstractPaperScissorsRockService {

    protected static final String ROCK = "R";
    protected static final String SCISSORS = "S";
    protected static final String PAPER = "P";
    protected static final List<String> VALID_MOVE = asList(ROCK, SCISSORS, PAPER);
    protected static final  String DRAW = "Game is Tie";
    protected static final String PLAY_AGAIN = "Play again? (Y)";
    protected static final String YES = "Y";
    protected static final String THANKS_FOR_THE_GAME_BYE = "Thanks for the game, bye!";
    protected static final String PLAYER_NAME = "You are playing with Computer.\nPlease Enter Player Name:";
    protected static final String WON = " Won";
    protected static final String MOVE = "'s Move?";
    protected static final String INVALID_MOVE = "Invalid move, Enter any one of R, P, S ";

    public abstract void playGame();

    protected final void showRules() {
        System.out.println("Welcome to Paper Scissors game and Rock game");
        System.out.println("Possible Moves/Values:\n 1. P for Paper \n 2. S Scissors \n 3. R for Rock\n");
    }

    protected final String findWinner(final Player player1, final Player player2) {
        final String value1 = player1.getValue();
        final String value2 = player2.getValue();
        String winner = null;

        if (ROCK.equalsIgnoreCase(value1)) {
            winner = SCISSORS.equalsIgnoreCase(value2) ? player1.getName() : player2.getName();
        } else if (PAPER.equalsIgnoreCase(value1)) {
            winner = ROCK.equalsIgnoreCase(value2) ? player1.getName() : player2.getName();
        } else {
            winner = PAPER.equalsIgnoreCase(value2) ? player1.getName() : player2.getName();
        }
        return winner;
    }
}

