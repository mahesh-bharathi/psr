package au.com.game;

import au.com.game.service.AbstractPaperScissorsRockService;
import au.com.game.service.PaperScissorsRockComputerService;

public class Main {

    public static void main(String args[]) {
        AbstractPaperScissorsRockService paperScissorsRockService = new PaperScissorsRockComputerService();
        paperScissorsRockService.playGame();
    }
}
