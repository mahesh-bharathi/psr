package au.com.game.utils;

import au.com.game.model.Player;

import static au.com.game.utils.PlayerType.*;

public class PlayerFactory {

    private static String COMPUTER = "Computer";

    //TODO: is static okay
    public static Player getPlayer(final PlayerType playerType, final String name) {
        final Player player = new Player();
        if(playerType == SYSTEM) {
            player.setName(COMPUTER);
        } else {
            player.setName(name);
        }
        player.setPlayerType(playerType);
        return player;
    }
}
