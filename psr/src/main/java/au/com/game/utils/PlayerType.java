package au.com.game.utils;

public enum PlayerType {
    SYSTEM,
    HUMAN;
}
